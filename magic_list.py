from dataclasses import dataclass




class MagicList(list):
    def __init__(self, cls_type=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if cls_type:
            self.append(cls_type)

    def __getitem__(self, item):
        pass

    def __setitem__(self, key, value):
        try:
            super().__setitem__(key, value)

        except IndexError as e:
            for idx in range(len(self), key + 1):
                self.append(None)
            self[key] = value

@dataclass
class Person:
    age: int = 1

### Q1
magic_class = MagicList()
magic_class[0] = 5
magic_class[1] = 6
print('magic_class: {}'.format(magic_class))

### Q2 - not working
magic_class = MagicList(cls_type=Person)
magic_class[0].age=5
print('magic_class: {}'.format(magic_class))

data_cls=Person()
pass